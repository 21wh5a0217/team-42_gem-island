def sum_of_total_gems(R, t):
    total_sum = 0
    for i in range(0,R):
        total_sum = total_sum + t[i]
    return(total_sum)

def sort(i):
    t = list(i)
    return sorted(t, reverse = True)

def totalgems(N, D, R, totalpermutations):
    total = 0
    for i in totalpermutations:
        t = sort(i)
        if sum(t) == N + D:
            total = total + sum_of_total_gems(R, t)
    return total