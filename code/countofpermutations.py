def countofpermutations(N, D, totalpermutations):
    count = 0
    for i in totalpermutations:
        t = sort(i)
        if sum(t) == N + D:
            count = count + 1
    return count
