Python 3.9.7 (tags/v3.9.7:1016ef3, Aug 30 2021, 20:19:38) [MSC v.1929 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license()" for more information.
>>> def expectedgems(N, D, R):
    if 1 <= N and D <= 500 and 1 <= R <= N:
        totalpermutations = total_permutations(N,D)
        total_gems = totalgems(N,D,R,totalpermutations)
        total_count = countofpermutations(N ,D ,totalpermutations)
    return(total_gems / total_count)
