def sum_of_total_gems(R, t):
    total_sum = 0
    for i in range(R):
        total_sum += t[i]
    return total_sum