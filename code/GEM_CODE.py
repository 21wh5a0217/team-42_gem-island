from itertools import product
def sum_of_total_gems(R, t):
    total_sum = 0
    for i in range(R):
        total_sum += t[i]
    return total_sum

def total_permutations(N, D):
    return list(product(list(range(1, D + 2)), repeat = N))

def sort(i):
    return sorted(list(i), reverse = True)

def totalgems(N, D, R, totalpermutations):
    total = 0
    for i in totalpermutations:
        if sum(sort(i)) == N + D:
            total += sum_of_total_gems(R, sort(i))
    return total

def countofpermutations(N, D, totalpermutations):
    count = 0
    for i in totalpermutations:
        if sum(sort(i)) == N + D:
            count += 1
    return count

def expectedgems(N, D, R):
    if 1 <= N and D <= 500 and 1 <= R <= N:
        total_gems = totalgems(N, D, R, total_permutations(N ,D))
        total_count = countofpermutations(N ,D , total_permutations(N, D))
    else :
        return "invalid input"
    return total_gems / total_count

print(expectedgems(2,3,1))
print(expectedgems(3,3,2))
print(expectedgems(5,10,3))
